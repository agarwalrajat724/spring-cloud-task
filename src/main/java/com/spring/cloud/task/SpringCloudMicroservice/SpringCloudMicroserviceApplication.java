package com.spring.cloud.task.SpringCloudMicroservice;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.task.configuration.EnableTask;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableTask
public class SpringCloudMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudMicroserviceApplication.class, args);
	}


	@Bean
	public TollProcessingTask tollProcessingTask() {
	    return new TollProcessingTask();
    }

	public class TollProcessingTask implements CommandLineRunner {

        @Override
        public void run(String... args) throws Exception {


            if (null != args) {
                System.out.println("Parameter Length is : " + args.length);

                String stationId = args[1];
                String licensePlate = args[2];
                String timestamp = args[3];

                System.out.println(" Station ID is : " + stationId + ", Plate is : " + licensePlate + ", timestamp is : " +timestamp);
            }
        }
    }
}
